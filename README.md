rainbot
=======

A Minecraft bot written with ScriptCraft; inspired by [wolfbot](https://github.com/cacciatc/wolfbot/)!

Installation
------------

1. Drag and drop the bot directory into your js-plugins folder.
2. Reload the CraftBukkit server.
3. Enjoy.

-c